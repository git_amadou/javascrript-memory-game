// eslint-disable-next-line no-unused-vars
const cartes = document.querySelectorAll('.memory-card')
const refresh = document.getElementById('refresh')
const close = document.getElementById('close')
const timeToGame = document.getElementById('timeToGame')
const infoSquareFind = document.getElementById('infoSquareFind')
const bestScores = document.getElementById('bestScores')
let hasFlippedCard = false
let lockBoard = false
let start = true
let firstCard
let secondCard
let timer = 0
let goodScore = 100
let scores = []

let cardSquareFind = 0
let intervalID

function flipCard () {
    if (lockBoard) return
    if (this === firstCard) return
    this.classList.add('flip')
    if (!hasFlippedCard) {
        refresh.innerHTML = 'Arreter'
        hasFlippedCard = true
        firstCard = this
    } else {
        hasFlippedCard = false
        secondCard = this
        if (firstCard.dataset.animal === secondCard.dataset.animal) {
            cardSquareFind++
            infoSquareFind.innerHTML = cardSquareFind
            firstCard.removeEventListener('click', flipCard)
            secondCard.removeEventListener('click', flipCard)
        } else {
            lockBoard = true
            setTimeout(() => {
                firstCard.classList.remove('flip')
                secondCard.classList.remove('flip');
                [hasFlippedCard, lockBoard] = [false, false];
                [firstCard, secondCard] = [null, null]
            }, 1000)
        }
    }
    if (cardSquareFind === 8) {
        start = false
        setTimeout(function () {
            alert('vous avez  mis ' + timer + 's' + ' pour trouver toutes les paires')
        }, 500)
        clearInterval(intervalID)
        timeToGame.innerHTML = timer
        scores.push(timer)
        if (timer < goodScore) {
            goodScore = timer
        }
        if (timer === goodScore) {
            alert(' Bravo! vous avez le meilleur score')
            bestScores.innerHTML = goodScore
        }
        scores.push(goodScore)
    }
}
cartes.forEach(function (card) {
    card.addEventListener('click', flipCard)
})
refresh.addEventListener('click', function () {
    if (!start) {
        cartes.forEach(function (card) {
            card.removeEventListener('click', flipCard)
        })
        refresh.innerHTML = 'Nouvelle Partie'
        infoSquareFind.innerHTML = cardSquareFind
        start = true
        lockBoard = false
        clearInterval(intervalID)
        timeToGame.innerHTML = timer
    } else {
        (function randomCard () {
            cartes.forEach(card => {
                let posRandom = Math.floor(Math.random() * 16)
                card.style.order = posRandom
            })
        })()
        setTimeout(() =>
            cartes.forEach(function (card) {
                card.classList.add('flip')
                console.log('Dans eventListener')
            }), 500)
        lockBoard = true
        setTimeout(() => {
            cartes.forEach(function (card) {
                card.addEventListener('click', flipCard)
                card.classList.toggle('flip')
            });
            [hasFlippedCard, lockBoard] = [false, false];
            [firstCard, secondCard] = [null, null]
        }, 2000)
        timer = 0
        intervalID = setInterval(function () {
            timer++
            timeToGame.innerHTML = timer
        }, 1000)
        cardSquareFind = 0
        infoSquareFind.innerHTML = cardSquareFind
        bestScores.innerHTML = goodScore
        refresh.innerHTML = 'Arreter la partie'
        start = false
    }
})
close.addEventListener('click', function () {
    window.close()
})
